<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    protected $table = 'pendaftaran';

    protected $fillable = ['tanggal_pendaftaran', 'status', 'siswa_idsiswa'];

    // Relasi dengan model Siswa
    public function siswa()
    {
        return $this->belongsTo('App\Models\Siswa', 'siswa_idsiswa');
    }
}
