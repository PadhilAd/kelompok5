<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    use HasFactory;

    protected $table = 'kelas';
    protected $fillable = [
        'tanggal_mulai',
        'tanggal_selesai',
        'kapasitas',
        'pendaftaran_idpendaftaran',
        'instruktur_idinstruktur',
        'kursus_idkursus',
    ];

    public function pendaftaran()
    {
        return $this->belongsTo(Pendaftaran::class, 'pendaftaran_idpendaftaran');
    }

    public function instruktur()
    {
        return $this->belongsTo(Instruktur::class, 'instruktur_idinstruktur');
    }

    public function kursus()
    {
        return $this->belongsTo(Kursus::class, 'kursus_idkursus');
    }
}
