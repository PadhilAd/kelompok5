<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    use HasFactory;
    protected $table= 'profile';
    protected $fillable= ['age' , 'bio' , 'users_id'];
}
