<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Instruktur;

class InstrukturController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $instrukturs = Instruktur::all();
        return view('instruktur.tampil', ['instrukturs' => $instrukturs]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('instruktur.tambah');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:instruktur,email',
            'nomor_telepon' => 'required|string|max:15',
            'keahlian' => 'required|string|max:255',
        ]);

        Instruktur::create($request->all());
        return redirect()->route('instruktur.index');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $instruktur = Instruktur::findOrFail($id);
        return view('instruktur.detail', ['instruktur' => $instruktur]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $instruktur = Instruktur::findOrFail($id);
        return view('instruktur.edit', ['instruktur' => $instruktur]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:instruktur,email,' . $id,
            'nomor_telepon' => 'required|string|max:15',
            'keahlian' => 'required|string|max:255',
        ]);

        $instruktur = Instruktur::findOrFail($id);
        $instruktur->update($request->all());
        return redirect()->route('instruktur.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Instruktur::findOrFail($id)->delete();
        return redirect()->route('instruktur.index');
    }
}
