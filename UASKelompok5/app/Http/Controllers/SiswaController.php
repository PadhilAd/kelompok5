<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $siswa1 = Siswa::all();
        return view('siswa.tampil', ['siswa1' => $siswa1]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('siswa.tambah');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required|min:5',
                'alamat' => 'required',
                'email' => 'required|email',
                'nomor_telepon' => 'required|numeric',
            ],
            [
                'name.required' => 'Nama tidak boleh kosong!',
                'alamat.required' => 'Alamat tidak boleh kosong!',
                'email.required' => 'Email tidak boleh kosong!',
                'nomor_telepon.required' => 'No hp tidak boleh kosong!',
                'nomor_telepon.numeric' => 'No hp harus berupa angka!',
            ]
        );

        $siswa1 = new Siswa;
        $siswa1->name = $request->input('name');
        $siswa1->alamat = $request->input('alamat');
        $siswa1->email = $request->input('email');
        $siswa1->nomor_telepon = $request->input('nomor_telepon');
        $siswa1->save();

        return redirect('/siswa')->with('success', 'Data siswa berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $siswa1 = Siswa::find($id);
        return view('siswa.detail', ['siswa1' => $siswa1]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $siswa1 = Siswa::findOrFail($id);
        return view('siswa.edit', ['siswa1' => $siswa1]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate(
            [
                'name' => 'required|min:5',
                'alamat' => 'required',
                'email' => 'required|email',
                'nomor_telepon' => 'required|numeric',
            ],
            [
                'name.required' => 'Nama tidak boleh kosong!',
                'alamat.required' => 'Alamat tidak boleh kosong!',
                'email.required' => 'Email tidak boleh kosong!',
                'email.email' => 'Email harus valid!',
                'nomor_telepon.required' => 'No hp tidak boleh kosong!',
                'nomor_telepon.numeric' => 'No hp harus berupa angka!',
            ]
        );

        $siswa1 = Siswa::findOrFail($id);
        $siswa1->name = $request->input('name');
        $siswa1->alamat = $request->input('alamat');
        $siswa1->email = $request->input('email');
        $siswa1->nomor_telepon = $request->input('nomor_telepon');
        $siswa1->save();

        return redirect('/siswa')->with('success', 'Data siswa berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $siswa1 = Siswa::findOrFail($id);
        $siswa1->delete();
        
        return redirect('/siswa')->with('success', 'Data siswa berhasil dihapus');
    }
}
