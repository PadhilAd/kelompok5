<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pendaftaran;
use App\Models\Siswa; // Tambahkan model Siswa

class PendaftaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pendaftarans = Pendaftaran::all();
        return view('pendaftaran.tampil', ['pendaftarans' => $pendaftarans]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $siswas = Siswa::all(); // Ambil semua data siswa
        return view('pendaftaran.tambah', ['siswas' => $siswas]); // Kirim data siswa ke view
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal_pendaftaran' => 'required|date',
            'status' => 'required',
            'siswa_idsiswa' => 'required|exists:siswa,id', // Pastikan siswa_idsiswa ada di tabel siswa
        ]);

        Pendaftaran::create($request->all());
        return redirect()->route('pendaftaran.index'); // Mengarahkan ke rute index
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $pendaftaran = Pendaftaran::findOrFail($id);
        return view('pendaftaran.detail', ['pendaftaran' => $pendaftaran]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $pendaftaran = Pendaftaran::findOrFail($id);
        $siswas = Siswa::all(); // Ambil semua data siswa
        return view('pendaftaran.edit', ['pendaftaran' => $pendaftaran, 'siswas' => $siswas]); // Kirim data siswa ke view
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tanggal_pendaftaran' => 'required|date',
            'status' => 'required',
            'siswa_idsiswa' => 'required|exists:siswa,id',
        ]);

        $pendaftaran = Pendaftaran::findOrFail($id);
        $pendaftaran->update($request->all());
        return redirect()->route('pendaftaran.index'); // Mengarahkan ke rute index
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Pendaftaran::findOrFail($id)->delete();
        return redirect()->route('pendaftaran.index'); // Mengarahkan ke rute index
    }
}