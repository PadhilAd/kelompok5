<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kursus;

class KursusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kursus = kursus::all();

        return view('kursus.tampil',['kursus' => $kursus]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view ('kursus.tambah');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request -> validate(
            [
                'materi' => 'required|min:5',
                'deskripsi' => 'required',

            ],
            [
                'materi.required' => 'Materi tidak boleh kosong!',
                'deskripsi.required' => 'Deskripsi tidak boleh kosong!',

            ]
        );

        $kursus = new kursus;

        $kursus->materi = $request->input('materi');
        $kursus->deskripsi = $request->input('deskripsi');

        $kursus->save();

        return redirect('/kursus');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $kursus = kursus::find($id);

        return view('kursus.detail',['kursus' => $kursus]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $kursus = kursus::find($id);

        return view('kursus.edit',['kursus' => $kursus]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request -> validate(
            [
                'materi' => 'required|min:5',
                'deskripsi' => 'required',

            ],
            [
                'materi.required' => 'Materi tidak boleh kosong!',
                'deskripsi.required' => 'Deskripsi tidak boleh kosong!',

            ]
        ); 
        
        kursus::where('id',$id)
        ->update(
            [

                'materi' => $request->input('materi'),
                'deskripsi' => $request->input('deskripsi'),

            ]
        );        
        return redirect('/kursus');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        kursus::where('id',$id)->delete();
       
        return redirect('/kursus');
    }
}
