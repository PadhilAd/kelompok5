<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\Pendaftaran;
use App\Models\Instruktur;
use App\Models\Kursus;

class KelasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kelases = Kelas::with('pendaftaran.siswa', 'instruktur', 'kursus')->get();
        return view('kelas.tampil', ['kelases' => $kelases]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $pendaftarans = Pendaftaran::where('status', 'Aktif')->with('siswa')->get();
        $instrukturs = Instruktur::all();
        $kursuses = Kursus::all();

        return view('kelas.tambah', [
            'pendaftarans' => $pendaftarans,
            'instrukturs' => $instrukturs,
            'kursuses' => $kursuses,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal_mulai' => 'required|date',
            'tanggal_selesai' => 'required|date|after:tanggal_mulai',
            'kapasitas' => 'required|integer|min:1',
            'pendaftaran_idpendaftaran' => 'required|exists:pendaftaran,id',
            'instruktur_idinstruktur' => 'required|exists:instruktur,id',
            'kursus_idkursus' => 'required|exists:kursus,id',
        ]);

        Kelas::create($request->all());
        return redirect()->route('kelas.index');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $kelas = Kelas::with('pendaftaran.siswa', 'instruktur', 'kursus')->findOrFail($id);
        return view('kelas.detail', ['kelas' => $kelas]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $kelas = Kelas::with('pendaftaran.siswa', 'instruktur', 'kursus')->findOrFail($id);
        $pendaftarans = Pendaftaran::where('status', 'Aktif')->with('siswa')->get();
        $instrukturs = Instruktur::all();
        $kursuses = Kursus::all();

        return view('kelas.edit', [
            'kelas' => $kelas,
            'pendaftarans' => $pendaftarans,
            'instrukturs' => $instrukturs,
            'kursuses' => $kursuses,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tanggal_mulai' => 'required|date',
            'tanggal_selesai' => 'required|date|after:tanggal_mulai',
            'kapasitas' => 'required|integer|min:1',
            'pendaftaran_idpendaftaran' => 'required|exists:pendaftaran,id',
            'instruktur_idinstruktur' => 'required|exists:instruktur,id',
            'kursus_idkursus' => 'required|exists:kursus,id',
        ]);

        $kelas = Kelas::findOrFail($id);
        $kelas->update($request->all());
        return redirect()->route('kelas.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Kelas::findOrFail($id)->delete();
        return redirect()->route('kelas.index');
    }
}
