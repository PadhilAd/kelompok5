<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kelas', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal_mulai');
            $table->date('tanggal_selesai');
            $table->integer('kapasitas');
            $table->unsignedBigInteger('pendaftaran_idpendaftaran');
            $table->foreign('pendaftaran_idpendaftaran')->references('id')->on('pendaftaran')->onDelete('cascade');
            $table->unsignedBigInteger('instruktur_idinstruktur');
            $table->foreign('instruktur_idinstruktur')->references('id')->on('instruktur')->onDelete('cascade');
            $table->unsignedBigInteger('kursus_idkursus');
            $table->foreign('kursus_idkursus')->references('id')->on('kursus')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kelas');
    }
};
