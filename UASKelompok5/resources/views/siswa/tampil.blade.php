@extends('layouts.master')

@section('content')
<a href="/siswa/create" class="btn btn-sm btn-primary">Tambah</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Siswa</th>
        <th scope="col">Alamat</th>
        <th scope="col">Email</th>
        <th scope="col">No Telepon</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($siswa1 as $key => $item)
      <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->name}}</td>
        <td>{{$item->alamat}}</td>
        <td>{{$item->email}}</td>
        <td>{{$item->nomor_telepon}}</td>
        <td>
            <form action="/siswa/{{$item->id}}" method="POST">
                <a href="/siswa/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/siswa/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method("Delete")
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
        </td>
      </tr>
      @empty
          <tr>
            <td>Tabel Siswa Belum Tersedia!</td>
          </tr>
      @endforelse
      
    </tbody>
  </table>
@endsection