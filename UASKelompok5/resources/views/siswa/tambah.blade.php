@extends('layouts.master')

@section('content')
<form method="POST" action="/siswa">
    {{-- Validation --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {{-- Form Input --}}
    @csrf
    <div class="form-group">
      <label for="name">Nama</label>
      <input type="text" class="form-control" id="name" name="name" required>
    </div>
    <div class="form-group">
      <label for="alamat">Alamat</label>
      <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="10" required></textarea>
    </div>
    <div class="form-group">
      <label for="email">Email</label>
      <input type="email" class="form-control" id="email" name="email" required>
    </div>
    <div class="form-group">
      <label for="nomor_telepon">No Telepon</label>
      <input type="text" class="form-control" id="nomor_telepon" name="nomor_telepon" required>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
