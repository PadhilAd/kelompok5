@extends('layouts.master')

@section('content')
<form method="POST" action="/siswa/{{$siswa1->id}}">
    {{-- Validation --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>           
    @endif
    
    {{-- Form Input --}}
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" value="{{$siswa1->name}}" class="form-control" name="name">
    </div>
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control" cols="30" rows="10">{{$siswa1->alamat}}</textarea>
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" value="{{$siswa1->email}}" class="form-control" name="email">
    </div>
    <div class="form-group">
        <label>No Telepon</label>
        <input type="text" value="{{$siswa1->nomor_telepon}}" class="form-control" name="nomor_telepon">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>   
@endsection
