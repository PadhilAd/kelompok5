@extends('layouts.master')

@section('content')
<h1>Detail Kelas</h1>
<p>Tanggal Mulai: {{ $kelas->tanggal_mulai }}</p>
<p>Tanggal Selesai: {{ $kelas->tanggal_selesai }}</p>
<p>Kapasitas: {{ $kelas->kapasitas }}</p>
<p>Siswa: {{ $kelas->pendaftaran->siswa->name }}</p>
<p>Instruktur: {{ $kelas->instruktur->nama }}</p>
<p>Kursus: {{ $kelas->kursus->materi }}</p>
@endsection
