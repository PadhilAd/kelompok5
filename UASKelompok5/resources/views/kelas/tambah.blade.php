@extends('layouts.master')

@section('content')
<form method="POST" action="{{ route('kelas.store') }}">
    @csrf

    <div class="form-group">
      <label>Tanggal Mulai</label>
      <input type="date" class="form-control" name="tanggal_mulai">
    </div>
    <div class="form-group">
      <label>Tanggal Selesai</label>
      <input type="date" class="form-control" name="tanggal_selesai">
    </div>
    <div class="form-group">
      <label>Kapasitas</label>
      <input type="number" class="form-control" name="kapasitas">
    </div>
    <div class="form-group">
      <label>Siswa</label>
      <select class="form-control" name="pendaftaran_idpendaftaran">
          @foreach($pendaftarans as $pendaftaran)
              <option value="{{ $pendaftaran->id }}">{{ $pendaftaran->siswa->name }}</option>
          @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Instruktur</label>
      <select class="form-control" name="instruktur_idinstruktur">
          @foreach($instrukturs as $instruktur)
              <option value="{{ $instruktur->id }}">{{ $instruktur->nama }}</option>
          @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Kursus</label>
      <select class="form-control" name="kursus_idkursus">
          @foreach($kursuses as $kursus)
              <option value="{{ $kursus->id }}">{{ $kursus->materi }}</option>
          @endforeach
      </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
