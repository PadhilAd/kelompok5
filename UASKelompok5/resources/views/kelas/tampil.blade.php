@extends('layouts.master')

@section('content')
@auth
<a href="{{ route('kelas.create') }}" class="btn btn-sm btn-primary">Tambah</a>
@endauth


<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Tanggal Mulai</th>
        <th scope="col">Tanggal Selesai</th>
        <th scope="col">Kapasitas</th>
        <th scope="col">Siswa</th>
        <th scope="col">Instruktur</th>
        <th scope="col">Kursus</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($kelases as $key => $kelas)
      <tr>
        <th scope="row">{{ $key + 1 }}</th>
        <td>{{ $kelas->tanggal_mulai }}</td>
        <td>{{ $kelas->tanggal_selesai }}</td>
        <td>{{ $kelas->kapasitas }}</td>
        <td>{{ $kelas->pendaftaran->siswa->name }}</td>
        <td>{{ $kelas->instruktur->nama }}</td>
        <td>{{ $kelas->kursus->materi }}</td>
        <td>
          <form action="{{ route('kelas.destroy', $kelas->id) }}" method="POST">
            <a href="{{ route('kelas.show', $kelas->id) }}" class="btn btn-info btn-sm">Detail</a>
          @auth
            <a href="{{ route('kelas.edit', $kelas->id) }}" class="btn btn-warning btn-sm">Edit</a>
            @csrf
            @method('DELETE')
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
        </form>
          @endauth
        </td>
      </tr>
      @empty
          <tr>
            <td colspan="8">Data Kelas Belum Tersedia!</td>
          </tr>
      @endforelse
    </tbody>
  </table>
@endsection
