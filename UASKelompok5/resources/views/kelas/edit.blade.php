@extends('layouts.master')

@section('content')
<form method="POST" action="{{ route('kelas.update', $kelas->id) }}">
    @csrf
    @method('PUT')

    <div class="form-group">
      <label>Tanggal Mulai</label>
      <input type="date" class="form-control" name="tanggal_mulai" value="{{ $kelas->tanggal_mulai }}">
    </div>
    <div class="form-group">
      <label>Tanggal Selesai</label>
      <input type="date" class="form-control" name="tanggal_selesai" value="{{ $kelas->tanggal_selesai }}">
    </div>
    <div class="form-group">
      <label>Kapasitas</label>
      <input type="number" class="form-control" name="kapasitas" value="{{ $kelas->kapasitas }}">
    </div>
    <div class="form-group">
      <label>Siswa</label>
      <select class="form-control" name="pendaftaran_idpendaftaran">
          @foreach($pendaftarans as $pendaftaran)
              <option value="{{ $pendaftaran->id }}" {{ $kelas->pendaftaran_idpendaftaran == $pendaftaran->id ? 'selected' : '' }}>{{ $pendaftaran->siswa->name }}</option>
          @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Instruktur</label>
      <select class="form-control" name="instruktur_idinstruktur">
          @foreach($instrukturs as $instruktur)
              <option value="{{ $instruktur->id }}" {{ $kelas->instruktur_idinstruktur == $instruktur->id ? 'selected' : '' }}>{{ $instruktur->nama }}</option>
          @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Kursus</label>
      <select class="form-control" name="kursus_idkursus">
          @foreach($kursuses as $kursus)
              <option value="{{ $kursus->id }}" {{ $kelas->kursus_idkursus == $kursus->id ? 'selected' : '' }}>{{ $kursus->materi }}</option>
          @endforeach
      </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
