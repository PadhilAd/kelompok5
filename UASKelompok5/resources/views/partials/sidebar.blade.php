<li class="nav-item active">
    <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Pages</span>
    </a>
    <div id="collapsePages" class="collapse show" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <ul class="nav nav-pills nav-sidebar flex-column" role="menu" data-accordion="false">
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Menu :
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/siswa" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Siswa</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/pendaftaran" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Siswa Yang Terdaftar</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/kelas" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Kelas</p>
              </a>
            </li>
            @auth
            <li class="nav-item">
              <a href="/kursus" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Kursus</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/instruktur" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Instruktur</p>
              </a>
            </li>
            @endauth
            @auth
            <div class="nav-item">
              <a class="nav-link" href="{{ route('logout') }}"
                 onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
              </form>
          </div>
            @endauth
            @guest
            <li class="nav-item">
              <a href="/login" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Login</p>
              </a>
            </li>
            @endguest
  
          </ul>
        </li>
      </ul>
    </div>
  </li>
  