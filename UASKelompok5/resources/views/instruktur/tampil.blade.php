@extends('layouts.master')

@section('content')
<a href="{{ route('instruktur.create') }}" class="btn btn-sm btn-primary">Tambah</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
        <th scope="col">No Telepon</th>
        <th scope="col">Keahlian</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($instrukturs as $key => $instruktur)
      <tr>
        <th scope="row">{{ $key + 1 }}</th>
        <td>{{ $instruktur->nama }}</td>
        <td>{{ $instruktur->email }}</td>
        <td>{{ $instruktur->nomor_telepon }}</td>
        <td>{{ $instruktur->keahlian }}</td>
        <td>
            <form action="{{ route('instruktur.destroy', $instruktur->id) }}" method="POST">
                <a href="{{ route('instruktur.show', $instruktur->id) }}" class="btn btn-info btn-sm">Detail</a>
                <a href="{{ route('instruktur.edit', $instruktur->id) }}" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
        </td>
      </tr>
      @empty
          <tr>
            <td colspan="6">Data Instruktur Belum Tersedia!</td>
          </tr>
      @endforelse
    </tbody>
  </table>
@endsection
