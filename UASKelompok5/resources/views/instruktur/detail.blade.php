@extends('layouts.master')

@section('content')
<h1>{{ $instruktur->nama }}</h1>
<p>Email: {{ $instruktur->email }}</p>
<p>No Telepon: {{ $instruktur->nomor_telepon }}</p>
<p>Keahlian: {{ $instruktur->keahlian }}</p>
@endsection
