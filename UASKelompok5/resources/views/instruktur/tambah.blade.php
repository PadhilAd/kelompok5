@extends('layouts.master')

@section('content')
<form method="POST" action="{{ route('instruktur.store') }}">
    @csrf

    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama">
    </div>
    <div class="form-group">
      <label>Email</label>
      <input type="email" class="form-control" name="email">
    </div>
    <div class="form-group">
      <label>No Telepon</label>
      <input type="text" class="form-control" name="nomor_telepon">
    </div>
    <div class="form-group">
      <label>Keahlian</label>
      <input type="text" class="form-control" name="keahlian">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
