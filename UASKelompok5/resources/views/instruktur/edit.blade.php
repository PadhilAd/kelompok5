@extends('layouts.master')

@section('content')
<form method="POST" action="{{ route('instruktur.update', $instruktur->id) }}">
    @csrf
    @method('PUT')

    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" value="{{ $instruktur->nama }}">
    </div>
    <div class="form-group">
      <label>Email</label>
      <input type="email" class="form-control" name="email" value="{{ $instruktur->email }}">
    </div>
    <div class="form-group">
      <label>No Telepon</label>
      <input type="text" class="form-control" name="nomor_telepon" value="{{ $instruktur->nomor_telepon }}">
    </div>
    <div class="form-group">
      <label>Keahlian</label>
      <input type="text" class="form-control" name="keahlian" value="{{ $instruktur->keahlian }}">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
