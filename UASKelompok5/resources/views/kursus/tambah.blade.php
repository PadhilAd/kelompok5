@extends('layouts.master')

@section('content')
<form method="POST" action="/kursus">
    {{-- Validation --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {{-- Form Input --}}
    @csrf
    <div class="form-group">
      <label > Materi </label>
      <input type="text" class="form-control" name="materi">
    </div>
    <div class="form-group">
      <label > Deskripsi</label>
      <textarea name="deskripsi" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection