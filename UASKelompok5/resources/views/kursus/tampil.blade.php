@extends('layouts.master')

@section('content')
<a href="/kursus/create" class="btn btn-sm btn-primary">Tambah</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Materi</th>
        <th scope="col">Deskripsi</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($kursus as $key => $item)
      <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->materi}}</td>
        <td>{{$item->deskripsi}}</td>
        <td>
            <form action="/kursus/{{$item->id}}" method="POST">
                <a href="/kursus/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/kursus/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method("Delete")
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
        </td>
      </tr>
      @empty
          <tr>
            <td>Tabel Cast Belum Tersedia!</td>
          </tr>
      @endforelse
      
    </tbody>
  </table>
@endsection