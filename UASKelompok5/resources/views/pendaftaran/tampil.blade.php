@extends('layouts.master')

@section('content')
@auth
<a href="{{ route('pendaftaran.create') }}" class="btn btn-sm btn-primary">Tambah</a>
@endauth


<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama Siswa</th>
            <th scope="col">Tanggal Pendaftaran</th>
            <th scope="col">Status</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($pendaftarans as $key => $pendaftaran)
        <tr>
            <th scope="row">{{ $key + 1 }}</th>
            <td>{{ $pendaftaran->siswa->name }}</td>
            <td>{{ $pendaftaran->tanggal_pendaftaran }}</td>
            <td>{{ $pendaftaran->status }}</td>
            <td>
                <form action="{{ route('pendaftaran.destroy', $pendaftaran->id) }}" method="POST">
                    <a href="{{ route('pendaftaran.show', $pendaftaran->id) }}" class="btn btn-info btn-sm">Detail</a>
                    @auth
                    <a href="{{ route('pendaftaran.edit', $pendaftaran->id) }}" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    @endauth
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="5">Tidak ada data pendaftaran</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection

