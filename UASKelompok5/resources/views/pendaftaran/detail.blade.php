@extends('layouts.master')

@section('content')
<h1>{{ $pendaftaran->siswa->username }}</h1>
<p>Tanggal Pendaftaran: {{ $pendaftaran->tanggal_pendaftaran }}</p>
<p>Status: {{ $pendaftaran->status }}</p>
@endsection
