@extends('layouts.master')

@section('content')
<form method="POST" action="{{ route('pendaftaran.store') }}">
    @csrf

    {{-- Validation Errors --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="form-group">
        <label for="tanggal_pendaftaran">Tanggal Pendaftaran</label>
        <input type="date" class="form-control" name="tanggal_pendaftaran" id="tanggal_pendaftaran">
    </div>

    <div class="form-group">
        <label for="status">Status</label>
        <select name="status" id="status" class="form-control">
            <option value="Aktif">Aktif</option>
            <option value="Tidak Aktif">Tidak Aktif</option>
        </select>
    </div>

    <div class="form-group">
        <label for="siswa_idsiswa">Siswa</label>
        <select name="siswa_idsiswa" id="siswa_idsiswa" class="form-control">
            @foreach($siswas as $siswa)
                <option value="{{ $siswa->id }}">{{ $siswa->name }}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
