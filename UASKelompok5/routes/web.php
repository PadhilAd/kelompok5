<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\KursusController;
use App\Http\Controllers\PendaftaranController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\InstrukturController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [homeController::class, 'index']);

Route::resource('siswa', SiswaController::class);
Route::resource('kursus', KursusController::class);
Route::resource('pendaftaran', PendaftaranController::class);
Route::resource('kelas', KelasController::class);
Route::resource('instruktur', InstrukturController::class);

Route::middleware(['auth'])->group(function () {
        Route::resource('kursus', KursusController::class);
        Route::resource('instruktur', InstrukturController::class);
        
});

Route::get('/master', function(){
    return view('layouts.master');
});

Auth::routes();
